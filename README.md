# Bunt Monorepo

This repository contains services and code built to successfully deploy
a scalable testing infrastructure for [bunt](https://gitlab.com/bunt-build/bunt).

## Repository Structure 

* `//data/...`: Generated data used for running a local testing environment.
* `//projects/...`: Project code. Each project can contain one or more module. A project should be a logical unit that defines a problem and a solution to that problem.
* `//tools/...`: Command line tools for the maintenence of the repo.


## Setting up a development environment

1. Clone this repository
2. Install `docker` and `docker-compose`
3. Run `docker-compose up -d --build` (https://bunt.localhost should now be running bunt)
4. Open your PyCharm IDE
5. Open each folder under `//projects/...` and "Attach" them to a single folder
6. Under `File > Settings > Project Interpreter` configure all of the projects to use the same venv.
7. Open each `requirements.txt` file and `setup.py` file. PyCharm should prompt you to install requirements. 
8. You should now have:
    * A fully self contained local environment for development running
    * An IDE configured for your projects
    * Full autocompletion on every project and library in the monorepo

