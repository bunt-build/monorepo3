from setuptools import setup

setup(
    name='ears',
    version='0.0.1',
    packages=['ears'],
    scripts=['bin/ears'],
    install_requires=['requests==2.21.0', 'pymongo==3.9.0', 'flask==1.1.1', 'waitress==1.3.1']
)
