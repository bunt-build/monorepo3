from abc import ABC, abstractmethod
from typing import Optional, List

from bson import ObjectId
from pymongo import MongoClient, ASCENDING, DESCENDING
from pymongo.collection import Collection
from pymongo.database import Database

from ears_communication import JobState


class JobStateRegistryException(Exception):
    ...


class JobStateNotFound(JobStateRegistryException):
    ...


class JobStateMalformed(JobStateRegistryException):
    ...


class JobStateRegistry(ABC):
    def find(self, job_status_id: str) -> JobState:
        if not self.exists(job_status_id):
            raise JobStateNotFound(f'No status with id {job_status_id} found')
        return self.load(job_status_id)

    def create(self, job_status: JobState) -> JobState:
        if job_status.id:
            raise JobStateMalformed('Cannot create a job status and provide the id it will have')

        if not job_status.job_id:
            raise JobStateMalformed('Must provide job id to a job status')

        if not job_status.status:
            raise JobStateMalformed('Must provide a status with a job status')

        return self.save(job_status)

    def latest(self, job_id: str) -> JobState:
        jobs = self.list(job_id=job_id, created_descending=True)
        if not jobs:
            raise JobStateNotFound(f'No status for {job_id} found')
        return jobs[0]

    @abstractmethod
    def load(self, job_status_id: str) -> JobState:
        """
        Load job status from storage.
        """

    @abstractmethod
    def save(self, job_status: JobState) -> JobState:
        """
        Save a job status. Overwrite if it exists
        """

    @abstractmethod
    def exists(self, job_status_id: str) -> bool:
        """
        Check if a job status exists
        """

    @abstractmethod
    def list(
            self,
            job_id: Optional[str] = None,
            created_ascending: bool = False, created_descending: bool = False,
            limit: Optional[int] = 100
    ) -> List[JobState]:
        """
        List job statuses
        """


class MongoJobStateRegistry(JobStateRegistry):
    def __init__(self, url: str):
        self.client = MongoClient(url)
        self.ears: Database = self.client.ears
        self.job_states: Collection = self.ears.jobstates

    def dict_to_mongo(self, data: dict) -> dict:
        if 'id' in data:
            if data['id']:
                data['_id'] = ObjectId(data['id'])
            del data['id']
        return data

    def mongo_to_dict(self, data: dict) -> dict:
        if '_id' in data:
            if data['_id']:
                data['id'] = str(data['_id'])
            del data['_id']
        return data

    def load(self, job_status_id: str) -> JobState:
        job_state_document = self.job_states.find_one({
            '_id': ObjectId(job_status_id),
        })

        if job_state_document is None:
            raise JobStateNotFound(f'Did not find job status with id {job_status_id}')

        return JobState.from_dict(self.mongo_to_dict(job_state_document))

    def save(self, job_status: JobState) -> JobState:
        result = self.job_states.insert_one(self.dict_to_mongo(job_status.to_dict()))
        job_status.id = str(result.inserted_id)
        return job_status

    def exists(self, job_status_id: str) -> bool:
        try:
            self.load(job_status_id)
            return True
        except JobStateNotFound:
            return False

    def list(
            self,
            job_id: Optional[str] = None,
            created_ascending: bool = False, created_descending: bool = False,
            limit: Optional[int] = 100
    ) -> List[JobState]:
        filters = {}
        orders = []

        if job_id:
            filters['jobId'] = job_id

        if created_ascending:
            orders.append(('_id', ASCENDING))
        elif created_descending:
            orders.append(('_id', DESCENDING))
        else:
            raise Exception('Cannot sort asc and desc on created!')

        job_states = self.job_states.find(
            filter=filters,
            sort=orders,
            limit=limit or 100
        )

        return [
            JobState.from_dict(self.mongo_to_dict(job_state))
            for job_state in job_states
        ]
