import Keycloak from 'keycloak-js';
import EarsClient from "./ears/EarsClient";
import WarrenClient from "./warren/WarrenClient";
import ColonyClient from "./colonies/ColonyClient";

export default class User {
    constructor(id, name, email, roles, userInfo, keycloak) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.roles = roles;
        this.userInfo = userInfo;
        this.keycloak = keycloak;

        this.ears = new EarsClient(this.keycloak);
        this.warren = new WarrenClient(this.keycloak);
        this.colony = new ColonyClient(this.keycloak);
    }

    getId() {
        return this.id;
    }

    /**
     * @returns {EarsClient}
     */
    getEarsClient() {
        return this.ears;
    }

    /**
     * @returns {WarrenClient}
     */
    getWarrenClient() {
        return this.warren;
    }

    /**
     * @returns {ColonyClient}
     */
    getColonyClient() {
        return this.colony;
    }

    isAuthenticated() {
        return this.keycloak.authenticated;
    }

    static async startLoginFlow() {
        const pathForKeycloakFile = process.env.REACT_APP_KEYCLOAK_PATH;
        const keycloak = new Keycloak(pathForKeycloakFile);
        const authenticated = await keycloak.init({
            onLoad: 'login-required',
            promiseType: 'native'
        });

        const userInfo = await keycloak.loadUserInfo();
        return new User(
            userInfo.sub,
            userInfo.name,
            userInfo.email,
            keycloak.realmAccess.roles,
            userInfo,
            keycloak
        );
    }
}
