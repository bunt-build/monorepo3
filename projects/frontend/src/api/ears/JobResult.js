import JobResultStatus from "./JobResultStatus";

export default class JobResult {
    constructor(id, testImageName, status) {
        this.id = id;
        this.testImageName = testImageName;
        this.status = status;

        if (![JobResultStatus.FAILED, JobResultStatus.SUCCESS].includes(status)) {
            throw new Error(status + ' is not a valid JobResultStatus')
        }
    }

    getId() {
        return this.id;
    }

    getTestImageName() {
        return this.testImageName;
    }

    getStatus() {
        return this.status;
    }

    static parse(data) {
        return new JobResult(
            data.id,
            data.testImageName,
            data.status,
        )
    }
}
