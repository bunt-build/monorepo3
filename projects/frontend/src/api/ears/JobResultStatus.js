const JobResultStatus = Object.freeze({
    FAILED: 'failed',
    SUCCESS: 'success',
});

export default JobResultStatus;
