import BackendClient from "../BackendClient";
import Job from "../warren/Job";
import JobState from "./JobState";
import JobResult from "./JobResult";

export default class EarsClient extends BackendClient {
    static url = process.env.REACT_APP_EARS_URL;

    /**
     * Get the results from a job.
     * @param job - Job to find results from
     * @returns {Promise<JobResult[]>}
     */
    async getJobResults(job) {
        const jobState = await this.getLatestJobState(job);
        return jobState.results;
    }

    async getLatestJobState(job) {
        const response = await this.fetch('/job-states', {
            params: {
                'equal[jobId]': job.getId(),
                'sort[created]': 'descending',
                'limit': 1
            }
        });
        const data = await response.json();
        const jobs = data.map(JobState.parse);
        if (jobs.length >= 1) {
            return jobs[0];
        } else {
            return null;
        }
    }
}
