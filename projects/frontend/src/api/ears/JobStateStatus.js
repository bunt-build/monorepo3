const JobStateStatus = Object.freeze({
    QUEUED: 'queued',
    RUNNING: 'running',
    FINISHED: 'finished',
});

export default JobStateStatus;
