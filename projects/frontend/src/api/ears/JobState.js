import JobStateStatus from "./JobStateStatus";
import JobResult from "./JobResult";

export default class JobState {
    constructor(id, jobId, status, results) {
        this.id = id;
        this.jobId = jobId;
        this.status = status;
        this.results = results; // TODO: Move results out of the JobState object.
    }

    getId() {
        return this.id;
    }

    getJobId() {
        return this.jobId;
    }


    getStatus() {
        return this.status;
    }

    static parse(data) {
        const validStates = [
            JobStateStatus.QUEUED,
            JobStateStatus.RUNNING,
            JobStateStatus.FINISHED,
        ];

        if (!validStates.includes(data.status))
            throw new Error(`Cannot parse JobState with id=${data.id}`);

        return new JobState(
            data.id,
            data.jobId,
            data.status,
            data.results.map(JobResult.parse)
        )
    }
}
