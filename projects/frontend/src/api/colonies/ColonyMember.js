import ColonyMemberRole from "./ColonyMemberRole";

export default class ColonyMember {
    constructor(id, colonyId, userId, role) {
        this.id = id;
        this.colonyId = colonyId;
        this.userId = userId;
        this.role = role;
    }

    getId() {
        return this.id;
    }

    getColonyId() {
        return this.colonyId;
    }

    getUserId() {
        return this.userId;
    }

    getRole() {
        return this.role;
    }

    static parse(data) {
        return new ColonyMember(
            data.id,
            data.colonyId,
            data.userId,
            ColonyMemberRole.parse(data.role)
        )
    }
}
