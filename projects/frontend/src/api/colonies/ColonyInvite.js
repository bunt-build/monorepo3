import ColonyMemberRole from "./ColonyMemberRole";

export default class ColonyInvite {
    constructor(id, colonyId, role) {
        this.id = id;
        this.colonyId = colonyId;
        this.role = role;
    }

    getId() {
        return this.id;
    }

    getColonyId() {
        return this.colonyId;
    }

    getRole() {
        return this.role;
    }

    static parse(data) {
        console.log(data);
        return new ColonyInvite(
            data.id,
            data.colonyId,
            ColonyMemberRole.parse(data.role)
        )
    }
}
