export default class ColonyMemberRole {
    static user = 'member';
    static admin = 'owner';

    static all = [ColonyMemberRole.user, ColonyMemberRole.admin];

    static parse(data) {
        data = data.toLocaleLowerCase();

        if (data === 'owner')
            return this.admin;

        if (data === 'member')
            return this.user;

        throw new Error('Could not parse ColonyMemberRole ' + data)
    }
}
