import BackendClient from "../BackendClient";
import Colony from "./Colony";
import ColonyMember from "./ColonyMember";
import ColonyMemberRole from "./ColonyMemberRole";
import ColonyInvite from "./ColonyInvite";
import ColonyToken from "./ColonyToken";

export default class ColonyClient extends BackendClient {
    static url = process.env.REACT_APP_COLONIES_URL;

    /**
     *
     * @param name
     * @returns {Promise<Colony>}
     */
    async createColony(name) {
        const response = await this.fetch(`/colonies`, {
            method: 'POST',
            data: {
                name: name
            }
        });
        const data = await response.json();
        return Colony.parse(data);
    }

    /**
     *
     * @param colony
     * @param role
     * @returns {Promise<ColonyInvite>}
     */
    async createColonyInvite(colony, role = ColonyMemberRole.user) {
        const response = await this.fetch(`/colony-invites`, {
            method: 'POST',
            data: {
                colonyId: colony.getId(),
                role: role,
            }
        });
        const data = await response.json();
        return ColonyInvite.parse(data);
    }


    /**
     *
     * @param colony
     * @returns {Promise<ColonyToken>}
     */
    async createColonyToken(colony) {
        const response = await this.fetch(`/colony-tokens`, {
            method: 'POST',
            data: {
                colonyId: colony.getId(),
            }
        });
        const data = await response.json();
        return ColonyToken.parse(data);
    }

    /**
     * @returns {Promise<ColonyMember>}
     */
    async useColonyInvite(colonyInviteId) {
        const response = await this.fetch(`/colony-invites/${colonyInviteId}/usages`, {
            method: 'POST',
            data: {}
        });
        const data = await response.json();
        return ColonyMember.parse(data);
    }

    /**
     * @returns {Promise<Colony[]>}
     */
    async getColonies() {
        const response = await this.fetch('/colonies');
        const data = await response.json();
        return data.map(Colony.parse);
    }

    /**
     *
     * @param colony Colony
     * @returns {Promise<Colony[]>}
     */
    async getColonyMembers(colony) {
        const colonyId = colony.getId();
        const response = await this.fetch(`/colonies/${colonyId}/members`);
        const data = await response.json();
        return data.map(ColonyMember.parse);
    }
}
