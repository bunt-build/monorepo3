export default class Colony {
    constructor(id, name, created) {
        this.id = id;
        this.name = name;
        this.created = created;
    }

    getId() {
        return this.id;
    }

    getName() {
        return this.name;
    }

    getCreated() {
        return this.created;
    }

    static parse(data) {
        console.log(data);
        return new Colony(
            data.id,
            data.name,
            new Date(data.created)
        );
    }
}
