export default class ColonyToken {
    constructor(id, colonyId) {
        this.id = id;
        this.colonyId = colonyId;
    }


    getId() {
        return this.id;
    }

    getColonyId() {
        return this.colonyId;
    }

    static parse(data) {
        return new ColonyToken(
            data.id,
            data.colonyId
        );
    }
}
