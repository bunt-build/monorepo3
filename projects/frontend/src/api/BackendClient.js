export default class BackendClient {
    static url = null;

    constructor(keycloak) {
        this.keycloak = keycloak;
    }

    fetch(path, query = undefined) {
        const options = query || {};
        let config = {
            method: options.method || 'GET',
            mode: 'cors',
            cache: 'default',
            credentials: 'omit', // include, *same-origin, omit
            headers: {
                "Authorization": "Bearer " + this.keycloak.token
            },
            redirect: 'follow',
            referrer: 'client',
        };


        if (options.data) {
            config.headers['Content-Type'] = 'application/json';
            config['body'] = JSON.stringify(options.data);
        }


        let queryString = '';
        if (options.params) {
            // This is awful
            queryString = '?' + Object.keys(options.params)
                .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(options.params[k]))
                .join('&');
        }

        return fetch('https://burrow.localhost' + path + queryString, config);
    }
}
