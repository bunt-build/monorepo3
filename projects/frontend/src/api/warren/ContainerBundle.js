export default class ContainerBundle {
    constructor(id) {
        this.id = id;
    }

    getId() {
        return this.id;
    }


    static parse(data) {
        return new ContainerBundle(data.id);
    }
}
