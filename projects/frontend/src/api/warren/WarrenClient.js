import BackendClient from "../BackendClient";
import Job from "./Job";

export default class WarrenClient extends BackendClient {
    static url = process.env.REACT_APP_WARREN_URL;

    async getJob(jobId) {
        const data = await this.fetch('/jobs/' + jobId).json();
        return Job.parse(data)
    }

    async getJobs() {
        const response = await this.fetch('/jobs');
        console.log(response);

        const data = await response.json();
        return data.map(Job.parse);
    }
}
