import ContainerBundle from "./ContainerBundle";

export default class JobContainer {
    constructor(tag, bundle) {
        this.tag = tag;
        if (bundle instanceof ContainerBundle) {
            this.bundle = bundle;
        } else {
            throw new Error('Bundle was not a ContainerBundle ' + bundle)
        }
    }


    static parse(data) {
        return new JobContainer(
            data.tag,
            ContainerBundle.parse(data.bundle)
        )
    }
}
