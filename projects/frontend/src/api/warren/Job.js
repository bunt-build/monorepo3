import JobContainer from "./JobContainer";

export default class Job {
    constructor(id, name, testImageNames, containers) {
        this.id = id;
        this.name = name;
        this.testImageNames = testImageNames;
        this.containers = containers;
    }

    getId() {
        return this.id;
    }

    getName() {
        return this.name;
    }

    static parse(data) {
        return new Job(
            data.id,
            data.name,
            data.testContainerNames,
            data.containers.map(JobContainer.parse)
        )
    }
}
