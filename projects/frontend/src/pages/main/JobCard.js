import React from 'react';
import Card from "react-bootstrap/Card";
import JobStateStatus from "../../api/ears/JobStateStatus";
import JobResultsListing from "./JobResultsListing";

export default class JobCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            job: this.props.job,
            user: this.props.user,
            jobState: null
        };

        this.monitor = null;
    }

    stopJobStatMonitor() {
        if (this.monitor === null)
            return;

        clearInterval(this.monitor);
        this.monitor = null;
    }

    queryJobState() {
        return this.state.user.getEarsClient().getLatestJobState(this.state.job).then(jobState => {
            this.setState({...this.state, jobState: jobState});

            if (jobState !== null && jobState.getStatus() === JobStateStatus.FINISHED) {
                this.stopJobStatMonitor();
            }
        });
    }

    componentDidMount() {
        this.monitor = setInterval(() => this.queryJobState(), 500);
    }

    componentWillUnmount() {
        this.stopJobStatMonitor();
    }

    render() {
        const job = this.state.job;
        const jobName = job.getName();

        let jobStatus;
        if (this.state.jobState !== null) {
            jobStatus = this.state.jobState.getStatus();
        } else {
            jobStatus = 'Loading...';
        }

        let jobResultsListing;
        if (this.state.jobState !== null) {
            jobResultsListing = <JobResultsListing
                user={this.state.user}
                job={this.state.job}
                jobState={this.state.jobState}
            />;
        } else {
            jobResultsListing = <p>Loading...</p>;
        }

        return <Card>
            <Card.Header>Name: {jobName}, Status: {jobStatus}</Card.Header>
            <Card.Body>

                {/*<Card.Title>Special title treatment</Card.Title>*/}
                <Card.Text>
                    {jobResultsListing}
                </Card.Text>
                {/*<Button variant="primary">Go somewhere</Button>*/}
            </Card.Body>
        </Card>
    }
}
