import React from 'react';
import JobCard from "./JobCard";

export default class HopList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {user: props.user, jobs: null};
    }

    componentDidMount() {
        if (!this.getUser()) {
            return;
        }

        this.getUser().getWarrenClient().getJobs().then(jobs => {
            this.setState({jobs: jobs})
        })
    }

    /**
     *
     * @returns User
     */
    getUser() {
        return this.state.user;
    }

    render() {
        const jobs = this.state.jobs;

        let jobCards;
        if (this.state.jobs) {
            jobCards = jobs.map(job => <JobCard job={job} user={this.state.user} />)
        } else {
            jobCards = <p>Loading...</p>
        }

        return (
            <div>
                {jobCards}
            </div>
        );
    }
}

