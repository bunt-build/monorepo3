import React from "react";

export default class JobResultsListing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            job: this.props.job,
            user: this.props.user,
            jobState: this.props.jobState,
            jobResults: [],
        };
    }

    componentDidMount() {
        this.state.user.getEarsClient().getJobResults(this.state.job).then(results => {
            this.setState({...this.state, jobResults: results})
        })
    }

    render() {
        return <div>
            {this.state.jobResults.map(jobResult =>
                <p>{jobResult.getTestImageName()}: {jobResult.getStatus()}</p>
            )}
        </div>;
    }
}
