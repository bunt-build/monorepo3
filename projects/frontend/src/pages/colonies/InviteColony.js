import React from "react";
import Button from "react-bootstrap/Button";
import ColonyMemberRole from "../../api/colonies/ColonyMemberRole";

export default class InviteColony extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            colony: props.colony,
        }
    }

    clicked() {
        this.state.user
            .getColonyClient()
            .createColonyInvite(this.state.colony, ColonyMemberRole.user)
            .then(invite => {
                navigator.clipboard.writeText(invite.getId()).then(() => alert('Copied to clipboard'));
            })
    }

    render() {
        return <div>
            <Button onClick={() => this.clicked()}>Create Invite</Button>
        </div>;
    }
}
