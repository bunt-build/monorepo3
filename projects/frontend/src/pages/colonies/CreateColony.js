import React from "react";
import Button from "react-bootstrap/Button";

export default class CreateColony extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user
        }
    }

    clicked(client) {
        const name = prompt('Colony Name?');
        this.state.user.getColonyClient().createColony(name).then(colony => console.log(colony));
    }

    render() {
        return <div>
            <Button onClick={() => this.clicked()}>Create Colony</Button>
        </div>;
    }
}
