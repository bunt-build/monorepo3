import React from "react";
import Button from "react-bootstrap/Button";

export default class JoinColony extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user
        }
    }

    clicked() {
        const inviteId = prompt('Colony Invite Token?');
        this.state.user.getColonyClient().useColonyInvite(inviteId).then(colony => console.log(colony));
    }

    render() {
        return <div>
            <Button onClick={() => this.clicked()}>Join Colony</Button>
        </div>;
    }
}
