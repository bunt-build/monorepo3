import React from "react";
import ColonyMemberRole from "../../api/colonies/ColonyMemberRole";
import Button from "react-bootstrap/Button";

export default class TokenColony extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            colony: props.colony,
        }
    }

    clicked() {
        this.state.user
            .getColonyClient()
            .createColonyToken(this.state.colony)
            .then(invite => {
                navigator.clipboard.writeText(invite.getId()).then(() => alert('Copied to clipboard'));
            })
    }

    render() {
        return <div>
            <Button onClick={() => this.clicked()}>Create Headless Token</Button>
        </div>;
    }
}
