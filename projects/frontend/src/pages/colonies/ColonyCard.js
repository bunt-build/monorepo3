import React from "react";
import Card from "react-bootstrap/Card";
import ColonyMemberRole from "../../api/colonies/ColonyMemberRole";
import InviteColony from "./InviteColony";
import TokenColony from "./TokenColony";

export default class ColonyCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            colony: this.props.colony,
            user: this.props.user,
            colonyMembers: []
        };

    }

    isUserAdmin() {
        return this.state.colonyMembers
            .filter(colonyMember => colonyMember.getRole() === ColonyMemberRole.admin)
            .map(colonyMember => colonyMember.getUserId())
            .includes(this.state.user.getId());
    }

    updateColonyMembers() {
        console.log(this.state.colony);
        return this.state.user.getColonyClient().getColonyMembers(this.state.colony).then(colonyMembers => {
            this.setState({...this.state, colonyMembers: colonyMembers});
        });
    }

    componentDidMount() {
        this.updateColonyMembers();
    }

    render() {

        let members = <p>Loading...</p>;
        if (this.state.colonyMembers) {
            members = this.state.colonyMembers.map(member => <div>
                {member.getId()}: {member.getRole()}
            </div>);
        }

        let createColonyTokenButton = '';
        let createInviteButton = '';
        if (this.isUserAdmin()) {
            createInviteButton = <InviteColony user={this.state.user} colony={this.state.colony}/>;
            createColonyTokenButton = <TokenColony user={this.state.user} colony={this.state.colony}/>;
        }
        return <Card>
            <Card.Header>Name: {this.state.colony.getName()},
                Created: {this.state.colony.getCreated().toISOString()}</Card.Header>
            <Card.Body>

                {/*<Card.Title>Special title treatment</Card.Title>*/}
                <Card.Text>
                    {createInviteButton} {createColonyTokenButton}
                    {members}
                </Card.Text>
                {/*<Button variant="primary">Go somewhere</Button>*/}
            </Card.Body>
        </Card>
    }
}
