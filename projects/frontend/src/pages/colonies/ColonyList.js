import React from "react";
import JobCard from "../main/JobCard";
import ColonyCard from "./ColonyCard";
import CreateColony from "./CreateColony";
import JoinColony from "./JoinColony";

export default class ColonyList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {user: props.user, colonies: null};
    }

    componentDidMount() {
        if (!this.getUser()) {
            return;
        }

        this.getUser().getColonyClient().getColonies().then(colonies => {
            this.setState({...this.state, colonies: colonies})
        })
    }

    /**
     *
     * @returns User
     */
    getUser() {
        return this.state.user;
    }

    render() {
        let colonyCards;
        if (this.state.colonies) {
            colonyCards = this.state.colonies.map(colony => <ColonyCard colony={colony} user={this.state.user}/>)
        } else {
            colonyCards = <p>Loading...</p>
        }

        return (
            <div>
                <CreateColony user={this.state.user}/>
                <JoinColony user={this.state.user}/>
                {colonyCards}
            </div>
        );
    }
}
