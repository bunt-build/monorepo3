import React from 'react';
import User from './api/User';

export default class SecuredApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {user: null};
    }

    componentDidMount() {
        User.getUser().then(user => this.setState({
            'user': user
        }))
    }

    render() {
        // User hasn't logged in.
        if (!this.state.user) {
            return (
                <div>Initializing Keycloak...</div>
            );
        }

        // User is not allowed to use this application
        if (!this.state.user.isAuthenticated()) {
            return (
                <div>Unable to authenticate!</div>
            );
        }

        return (
            <div>
                <p>This is a Keycloak-secured component of your application. You shouldn't be able
                    to see this unless you've authenticated with Keycloak.</p>
            </div>
        );
    }
}
