import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import User from "./api/User";
import {
    Switch,
    Route,
    Link
} from "react-router-dom";
import Container from "react-bootstrap/Container";
import HopList from "./pages/main/HopList";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import NavItem from "react-bootstrap/NavItem";
import ColonyList from "./pages/colonies/ColonyList";

function About() {
    return <div>about</div>
}

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {user: null};
    }

    componentDidMount() {
        User.startLoginFlow().then(user => {
            this.setState({
                user: user
            })
        })
    }

    render() {
        if (!this.state.user) {
            return (
                <div>
                    <p>Loading...</p>
                </div>
            );
        }

        if (!this.state.user.isAuthenticated()) {
            return (
                <div>
                    <p>Not allowed to access this site!</p>
                </div>
            );
        }


        const routes = [
            {path: '/', title: 'Hops', component: <HopList user={this.state.user}/>},
            {path: '/colonies', title: 'Colonies', component: <ColonyList user={this.state.user}/>},
        ];

        return (
            <div id={"root"}>
                <Container>
                    <div>
                        <Navbar>
                            <Navbar.Brand as={Link} to="/">Bunt</Navbar.Brand>
                            <Navbar.Collapse>
                                {routes.map(r =>
                                    <Nav>
                                        <NavItem href={r.path}>
                                            <Nav.Link as={Link} to={r.path}>
                                                {r.title}
                                            </Nav.Link>
                                        </NavItem>
                                    </Nav>
                                )}
                            </Navbar.Collapse>
                        </Navbar>
                        <Switch>
                            {routes.map(r => <Route exact path={r.path}>{r.component}</Route>)}
                            <Route render={function () {
                                return <p>Page not found.</p>
                            }}/>
                        </Switch>
                    </div>
                </Container>

            </div>
        );
    }
}

