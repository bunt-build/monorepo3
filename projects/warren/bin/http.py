import signal
import sys
from pathlib import Path
from tempfile import mktemp

from flask import Flask, request, jsonify, Response, stream_with_context, abort
from waitress import serve

from ears_communication import EarsClient, HttpEarsClient, JobState, JobStateStatus
from warren.bundles import ContainerBundleRegistry, DiskBinaryContainerBundleRegistry, ContainerBundleNotFound
from warren.communication import Job, RedisQueueWarren
from warren.jobs import JobRegistry, MongoJobRegistry, JobRegistryNotFound
from warren.permissions import UserSession, Session

app = Flask(__name__)
job_registry: JobRegistry = MongoJobRegistry('mongodb://root:root@mongo')  # TODO(jkatz): Config
container_bundle_registry: ContainerBundleRegistry = DiskBinaryContainerBundleRegistry(Path('/data/containers'))
queue = RedisQueueWarren('redis://redis')  # TODO(jkatz): Config
ears_client: EarsClient = HttpEarsClient('http://ears')  # TODO(jkatz): Config


class TempDiskBacking:
    path: Path

    def handle(self):
        return self.path.open('rb')

    def fill_with(self, chunk_size: int = 1024):
        with self.path.open('wb') as handle:
            for chunk in iter(lambda: request.stream.read(chunk_size), b''):
                handle.write(chunk)

    def __enter__(self):
        self.path = Path(mktemp())
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.path.unlink()


def signal_handler(sig, frame):
    print('Received signal:', sig)
    sys.exit(0)


@app.route('/jobs', methods=['POST'])
def jobs_create():
    job = Job.from_dict(request.json)

    if not Session.load().can_create_jobs_for(job.colony_id):
        raise abort(401, 'Cannot create job for another colony')

    job = job_registry.create(job)
    ears_client.transition_state(JobState(
        id=None,
        job_id=job.id,
        status=JobStateStatus.QUEUED,
        results=[],
    ))
    queue.push(job)
    return jsonify(job.to_dict())


@app.route('/jobs', methods=['GET'])
def jobs_list():
    visible_colonies = Session.load().visible_colonies_filter()
    print('Visible Colonies: ', visible_colonies)
    jobs = job_registry.list(visible_colonies)
    return jsonify([job.to_dict() for job in jobs])


@app.route('/jobs/<job_id>', methods=['GET'])
def jobs_get(job_id: str):
    try:
        job = job_registry.get(job_id)
    except JobRegistryNotFound:
        raise abort(404, 'Job not found.')
    except:
        raise abort(500, 'Unknown internal error.')

    if Session.load().can_get_jobs_for(job.colony_id):
        raise abort(401, 'Cannot read this job.')

    return jsonify(job.to_dict())


@app.route('/container-bundles', methods=['POST'])
def container_bundles_create():
    with TempDiskBacking() as backing:
        backing.fill_with()
        with backing.handle() as handle:
            container_bundle = container_bundle_registry.create(handle)

    return jsonify(container_bundle.to_dict())


@app.route('/container-bundles/<container_bundle_id>', methods=['GET'])
def container_bundles_get(container_bundle_id: str):
    if not container_bundle_registry.exists(container_bundle_id):
        raise abort(404, 'Container bundle not found.')

    try:
        return container_bundle_registry.find(container_bundle_id)
    except ContainerBundleNotFound:
        raise abort(404, 'Container bundle not found.')
    except:
        raise abort(500, 'Unknown internal error.')


@app.route('/container-bundles/<container_bundle_id>/contents', methods=['GET'])
def container_bundles_contents_get(container_bundle_id: str):
    if not container_bundle_registry.exists(container_bundle_id):
        raise abort(404, 'Container bundle not found.')

    container_bundle = container_bundle_registry.find(container_bundle_id)

    return Response(
        stream_with_context(container_bundle_registry.get_contents_generator(container_bundle)),
        content_type='application/x-tar'
    )


signal.signal(signal.SIGINT, signal_handler)
app.run(debug=True, host='0.0.0.0', port=80)
#serve(app, host='0.0.0.0', port=80)
