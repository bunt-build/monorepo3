import json
import logging
from traceback import print_exc

import requests
from typing import List, Set, Optional

from redis import Redis


class WarrenException(Exception):
    ...


class ContainerBundle:
    def __init__(self, container_bundle_id: str):
        self.id = container_bundle_id

    def to_dict(self) -> dict:
        return {'id': self.id}

    @classmethod
    def from_dict(cls, data: dict):
        return ContainerBundle(container_bundle_id=data['id'])


class JobContainer:
    def __init__(self, tag: str, bundle: ContainerBundle):
        self.tag = tag
        self.bundle = bundle

    def to_dict(self) -> dict:
        return {
            'tag': self.tag,
            'bundle': self.bundle.to_dict(),
        }

    @classmethod
    def from_dict(cls, data: dict):
        return JobContainer(
            tag=data['tag'],
            bundle=ContainerBundle.from_dict(data['bundle']),
        )


class Job:
    def __init__(
            self,
            job_id: Optional[str],
            name: str,
            test_container_names: List[str],
            containers: List[JobContainer],
            colony_id: str
    ):
        self.id: Optional[str] = job_id
        self.name: str = name
        self.test_container_names = test_container_names
        self.containers: List[JobContainer] = containers
        self.colony_id = colony_id

    def bundles(self) -> Set[str]:
        """
        All of the bundles we need to import in order to import to obtain all of the desired containers
        """
        return set(container.bundle.id for container in self.containers)

    def to_dict(self):
        data = {
            'name': self.name,
            'testContainerNames': self.test_container_names,
            'containers': [container.to_dict() for container in self.containers],
            'colonyId': self.colony_id
        }
        if self.id is not None:
            data['id'] = self.id
        return data

    @classmethod
    def from_dict(cls, data):
        job_id = data['id'] if 'id' in data else None
        name = data['name']
        test_container_names = data['testContainerNames']
        containers = [JobContainer.from_dict(container) for container in data['containers']]
        return Job(
            job_id=job_id,
            name=name,
            test_container_names=test_container_names,
            containers=containers,
            colony_id=data['colonyId']
        )


class RedisQueueWarren:
    def __init__(self, url: str):
        self.client = Redis.from_url(url)
        self.redis_key = 'queue:hops'

    @staticmethod
    def encode(job: Job):
        return json.dumps(job.to_dict())

    @staticmethod
    def decode(data: bytes) -> Job:
        decoded = data.decode()
        parsed = json.loads(decoded)
        return Job.from_dict(parsed)

    def push(self, job: Job):
        self.client.rpush(self.redis_key, RedisQueueWarren.encode(job))

    def pull(self):
        while True:
            key, data = self.client.blpop([self.redis_key])

            try:
                return RedisQueueWarren.decode(data)
            except:
                print_exc()
                logging.error(f'Job received on queue {key} failed to parse: {data}')
                continue


class HttpClientWarren:
    def __init__(self, url: str, token_id: Optional[str] = None):
        self.url = url
        self.token_id = token_id

    def headers(self) -> dict:
        if self.token_id:
            return {
                'Authorization': self.token_id
            }
        else:
            return {}

    def container_bundle_create(self, file_like) -> ContainerBundle:
        response = requests.post(f'{self.url}/container-bundles', data=file_like, headers=self.headers())

        if not response:
            raise WarrenException(response.content)

        return ContainerBundle.from_dict(response.json())

    def container_bundle_get(self, container_bundle_id: str) -> ContainerBundle:
        response = requests.get(f'{self.url}/container-bundles/{container_bundle_id}', headers=self.headers())

        if not response:
            raise WarrenException(response.content)

        return ContainerBundle.from_dict(response.json())

    def container_bundle_contents_get(self, container_bundle_id: str, chunk_size: int = 8192) -> ContainerBundle:
        url = f'{self.url}/container-bundles/{container_bundle_id}/contents'
        with requests.get(url, stream=True, headers=self.headers()) as r:
            if not r:
                raise WarrenException(r.content)

            for chunk in r.iter_content(chunk_size=chunk_size):
                yield chunk

    def job_create(self, job: Job) -> Job:
        response = requests.post(f'{self.url}/jobs', json=job.to_dict(), headers=self.headers())

        if not response:
            raise WarrenException(response.content)

        return Job.from_dict(response.json())

    def job_update(self, job: Job) -> Job:
        response = requests.put(f'{self.url}/jobs/{job.id}', json=job.to_dict(), headers=self.headers())

        if not response:
            raise WarrenException(response.content)

        return Job.from_dict(response.json())

    def job_get(self, job_id: str) -> Job:
        response = requests.get(f'{self.url}/jobs/{job_id}', headers=self.headers())

        if not response:
            raise WarrenException(response.content)

        return Job.from_dict(response.json())
