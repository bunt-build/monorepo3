from abc import ABC, abstractmethod
from hashlib import sha1
from pathlib import Path
from shutil import copyfileobj
from typing import BinaryIO

from warren.communication import ContainerBundle


class ContainerBundleException(Exception):
    ...


class ContainerBundleNotFound(ContainerBundleException):
    ...


class ContainerBundleRegistry(ABC):

    def hash_io_stream(self, handle: BinaryIO, algorithm, chunk_size: int = 8192) -> str:
        handle.seek(0)
        consumer = algorithm()
        for chunk in iter(lambda: handle.read(chunk_size), b''):
            consumer.update(chunk)
        return consumer.hexdigest()

    def create(self, handle: BinaryIO):
        container_bundle: ContainerBundle = ContainerBundle(
            container_bundle_id=self.hash_io_stream(handle, sha1)
        )

        if self.exists(container_bundle.id):
            return container_bundle
        else:
            return self.set(container_bundle.id, handle)

    @abstractmethod
    def find(self, container_bundle_id: str) -> ContainerBundle:
        ...

    @abstractmethod
    def exists(self, container_bundle_id: str) -> bool:
        ...

    @abstractmethod
    def set(self, container_bundle_id: str, handle: BinaryIO) -> ContainerBundle:
        ...

    def get_contents_generator(self, container_bundle: ContainerBundle):
        with self.get_contents(container_bundle) as handle:
            handle.seek(0)
            for chunk in iter(lambda: handle.read(8 * 1024), b''):
                yield chunk

    @abstractmethod
    def get_contents(self, container_bundle: ContainerBundle) -> BinaryIO:
        ...


class DiskBinaryContainerBundleRegistry(ContainerBundleRegistry):
    def __init__(self, folder: Path):
        self.folder = folder
        self.folder.mkdir(parents=True, exist_ok=True)

    def __bundle_file(self, container_bundle_id: str) -> Path:
        return self.folder / (container_bundle_id + '.tar')

    def exists(self, container_bundle_id: str) -> bool:
        return self.__bundle_file(container_bundle_id).exists()

    def find(self, container_bundle_id: str) -> ContainerBundle:
        if not self.exists(container_bundle_id):
            raise ContainerBundleException(f'Could not find bundle {container_bundle_id}')

        return ContainerBundle(
            container_bundle_id=container_bundle_id,
        )

    def set(self, container_bundle_id: str, handle: BinaryIO) -> ContainerBundle:
        handle.seek(0)
        with self.__bundle_file(container_bundle_id).open('wb') as disk_handle:
            copyfileobj(handle, disk_handle)
        return ContainerBundle(
            container_bundle_id=container_bundle_id
        )

    def get_contents(self, container_bundle: ContainerBundle) -> BinaryIO:
        return self.__bundle_file(container_bundle.id).open('rb')
