from abc import ABC, abstractmethod
from typing import Optional, List

from flask import request

from colonies_communication import HttpColonyClient


class Session(ABC):

    @abstractmethod
    def visible_colonies_filter(self) -> Optional[List[str]]:
        ...

    @abstractmethod
    def can_create_jobs_for(self, colony_id: str) -> bool:
        ...

    @abstractmethod
    def can_list_jobs_for(self, colony_id: str) -> bool:
        ...

    def can_get_jobs_for(self, colony_id: str) -> bool:
        return self.can_list_jobs_for(colony_id)

    @staticmethod
    def load() -> 'Session':
        if 'x-bunt-gateway-headless-id' in request.headers:
            print('Headless')
            # Headless API key
            return HeadlessSession()
        if 'x-bunt-gateway-user-id' in request.headers:
            print('User')
            # User API key
            return UserSession()

        print('System')
        # Backend RPC
        return SystemSession()


class UserSession(Session):
    def __init__(self):
        self.colonies_client = HttpColonyClient('http://colonies')  # TODO: Make configurable

    @property
    def id(self) -> str:
        return request.headers['x-bunt-gateway-user-id']

    def is_member_of_colony(self, colony_id: str) -> bool:
        return colony_id in self.visible_colonies_filter()

    def visible_colonies_filter(self) -> List[str]:
        return [colony.id for colony in self.colonies_client.visible_to(self.id)]

    def can_create_jobs_for(self, colony_id: str) -> bool:
        return self.is_member_of_colony(colony_id)

    def can_list_jobs_for(self, colony_id: str) -> bool:
        return self.is_member_of_colony(colony_id)


class HeadlessSession(Session):
    @property
    def colony(self) -> str:
        return request.headers['x-bunt-gateway-headless-colony']

    def can_create_jobs_for(self, colony_id: str) -> bool:
        print('Comparing', colony_id, 'to', self.colony)
        # Can only create jobs for your own colony
        return self.colony == colony_id

    def can_list_jobs_for(self, colony_id: str) -> bool:
        print('Checking', self.colony, 'against', colony_id)
        return self.colony == colony_id

    def visible_colonies_filter(self) -> List[str]:
        return [self.colony]


class SystemSession(Session):
    def can_list_jobs_for(self, colony_id: str) -> bool:
        return True

    def can_create_jobs_for(self, colony_id: str) -> bool:
        return True

    def visible_colonies_filter(self) -> Optional[List[str]]:
        return None
