from abc import ABC, abstractmethod
from typing import List, Optional

from bson import ObjectId
from pymongo import MongoClient
from pymongo.collection import Collection
from pymongo.database import Database

from warren.communication import Job


class JobRegistryException(Exception):
    ...


class JobRegistryCannotBeRecreated(JobRegistryException):
    ...


class JobRegistryNotFound(JobRegistryException):
    ...


class JobRegistry(ABC):
    def create(self, job: Job) -> Job:
        if job.id:
            raise JobRegistryCannotBeRecreated('Attempted to create a job while also specifying the Job\'s ID')

        return self.set(Job(
            job_id=None,
            name=job.name,
            test_container_names=list(job.test_container_names),
            containers=list(job.containers),
            colony_id=job.colony_id,
        ))

    # def update(self, job: Job) -> Job:
    #     # TODO: compare and only update updatable items
    #     # original = self.get(job.id)
    #     return self.set(job)

    @abstractmethod
    def list(self, visible_colonies_filter: Optional[List[str]]) -> List[Job]:
        """
        List jobs
        :parameter visible_colonies_filter: Optional filter for colony jobs. If None no filter is applied
        :return:
        """

    @abstractmethod
    def set(self, job: Job) -> Job:
        """
        Set the stored contents of a job to a provided state.
        :param job:
        :return:
        """

    @abstractmethod
    def get(self, job_id: str) -> Job:
        """
        Get a job.
        :param job_id:
        :return:
        """


class MongoJobRegistry(JobRegistry):
    def __init__(self, url: str):
        self.client = MongoClient(url)
        self.warren: Database = self.client.warren
        self.jobs: Collection = self.warren.jobs

    def mongo_to_data(self, document: dict) -> dict:
        data = {
            **document
        }

        if '_id' in document and document['_id']:
            data['id'] = str(document['_id'])

        return data

    def data_to_mongo(self, data: dict) -> dict:
        document = {
            **data
        }

        if 'id' in data and data['id']:
            document['_id'] = ObjectId(document['_id'])

        return data

    def list(self, visible_colonies_filter: Optional[List[str]]) -> List[Job]:
        filters = {}

        if visible_colonies_filter is not None:
            filters['colonyId'] = {'$in': visible_colonies_filter}

        job_documents = self.jobs.find(filter=filters)
        return [
            Job.from_dict(self.mongo_to_data(job))
            for job in job_documents
        ]

    def set(self, job: Job) -> Job:
        document = self.data_to_mongo(job.to_dict())

        if '_id' in document and document['_id']:
            self.jobs.update_one(
                filter={'_id': document['_id']},
                update={k: v for k, v in document.items() if k != '_id'},
                upsert=False,
            )
            job_id = job.id
        else:
            result = self.jobs.insert_one(document)
            job_id = str(result.inserted_id)

        return self.get(job_id)

    def get(self, job_id: str) -> Job:
        job_document = self.jobs.find_one({'_id': ObjectId(job_id)})
        if job_document is None:
            raise JobRegistryNotFound('Job not found with ID ' + job_id)
        return Job.from_dict(self.mongo_to_data(job_document))
