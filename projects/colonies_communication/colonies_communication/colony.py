from datetime import datetime
from enum import Enum
from typing import Optional


class Colony:
    def __init__(self, colony_id: Optional[str], name: str, created: datetime):
        """
        :param colony_id: Unique identifier. None if this object has not been created.
        :param name: Human-friendly name of the colony.
        :param created: Date this was created
        """
        self.id = colony_id
        self.name = name
        self.created = created

    def to_dict(self) -> dict:
        data = {
            'name': self.name,
            'created': self.created.isoformat(),
        }
        if self.id:
            data['id'] = self.id
        return data

    @classmethod
    def from_dict(cls, data: dict) -> 'Colony':
        if 'created' in data and data['created']:
            created = datetime.fromisoformat(data['created'])
        else:
            created = datetime.utcnow()
        return Colony(
            colony_id=data['id'] if 'id' in data and data['id'] else None,
            name=data['name'],
            created=created,
        )


class ColonyMemberRole(Enum):
    MEMBER = 'member'
    ADMIN = 'owner'


class ColonyMember:
    def __init__(self, colony_member_id: Optional[str], colony_id: str, user_id: str, role: ColonyMemberRole):
        """
        Member of a colony.
        :param colony_member_id: ID of this membership representation
        :param colony_id: Colony this member exists in.
        :param user_id: User ID
        :param role: Role of the member
        """
        self.id = colony_member_id
        self.colony_id = colony_id
        self.user_id = user_id
        self.role = role

    def to_dict(self) -> dict:
        data = {
            'colonyId': self.colony_id,
            'userId': self.user_id,
            'role': self.role.value,
        }
        if self.id:
            data['id'] = self.id
        return data

    @classmethod
    def from_dict(cls, data: dict) -> 'ColonyMember':
        return ColonyMember(
            colony_member_id=data['id'] if 'id' in data and data['id'] else None,
            user_id=data['userId'],
            colony_id=data['colonyId'],
            role=ColonyMemberRole(data['role'])
        )
