from typing import Optional

from colonies_communication.colony import ColonyMemberRole


class ColonyInvite:
    def __init__(self, colony_invite_id: Optional[str], colony_id: str, role: ColonyMemberRole):
        self.id = colony_invite_id
        self.colony_id = colony_id
        self.role = role

    def to_dict(self) -> dict:
        data = {
            'colonyId': self.colony_id,
            'role': self.role.value,
        }

        if self.id:
            data['id'] = self.id

        return data

    @classmethod
    def from_dict(cls, data: dict) -> 'ColonyInvite':
        return ColonyInvite(
            colony_invite_id=data['id'] if 'id' in data and data['id'] else None,
            colony_id=data['colonyId'],
            role=ColonyMemberRole(data['role'])
        )
