from itertools import chain
from typing import List, Optional

import requests

from colonies_communication.invite import ColonyInvite
from colonies_communication.colony import Colony, ColonyMember, ColonyMemberRole
from colonies_communication.tokens import ColonyToken


class HttpColonyClient:
    def __init__(self, url: str, token_id: Optional[str] = None):
        self.url = url
        self.token_id = token_id

    def headers(self) -> dict:
        if self.token_id:
            return {
                'Authorization': self.token_id
            }
        else:
            return {}

    def visible_to(self, user_id: str) -> List[Colony]:
        """
        List all of the colonies visible to a user. This means they must be
        a member of this colony.
        :param user_id: ID of a user.
        :return:
        """
        response = requests.get(f'{self.url}/colonies', params={
            'contains[userId]': user_id
        }, headers=self.headers())
        return [
            Colony.from_dict(colony)
            for colony in response.json()
        ]

    def members(self, colony_id: str) -> List[ColonyMember]:
        response = requests.get(f'{self.url}/colonies/{colony_id}/members', headers=self.headers())
        return [
            ColonyMember.from_dict(colony_member)
            for colony_member in response.json()
        ]

    def create_invite(self, colony_id: str, role: ColonyMemberRole) -> ColonyInvite:
        response = requests.post(f'{self.url}/colony-invites', json={
            'colonyId': colony_id,
            'role': role.value
        }, headers=self.headers())
        return ColonyInvite.from_dict(response.json())

    def use_invite(self, invite_id: str, user_id: str) -> bool:
        response = requests.post(f'{self.url}/colony-invites/{invite_id}/usages', headers={
            'x-bunt-gateway-user-id': user_id,
            **self.headers()
        })
        if response:
            return True
        else:
            return False

    def create_token(self, colony: Colony) -> ColonyToken:
        response = requests.post(f'{self.url}/colony-tokens', json={
            'colonyId': colony.id
        }, headers=self.headers())
        return ColonyToken.from_dict(response.json())

    def list_tokens(self, colony: Colony) -> List[ColonyToken]:
        response = requests.post(f'{self.url}/colony-tokens', params={
            'equal[colonyId]': colony.id
        }, headers=self.headers())
        return [ColonyToken.from_dict(token) for token in response.json()]

    def get_token(self, colony_token_id: str) -> ColonyToken:
        response = requests.get(f'{self.url}/colony-tokens/{colony_token_id}', headers=self.headers())
        return ColonyToken.from_dict(response.json())

    def tokens_visible_to(self, user_id: str) -> List[ColonyToken]:
        return list(chain.from_iterable([
            self.list_tokens(colony)
            for colony in self.visible_to(user_id)
        ]))
