from typing import Optional


class ColonyToken:
    def __init__(self, colony_token_id: Optional[str], colony_id: str):
        self.id = colony_token_id
        self.colony_id = colony_id

    def to_dict(self) -> dict:
        data = {
            'colonyId': self.colony_id,
        }

        if self.id:
            data['id'] = self.id

        return data

    @classmethod
    def from_dict(cls, data: dict) -> 'ColonyToken':
        return ColonyToken(
            colony_token_id=data['id'] if 'id' in data and data['id'] else None,
            colony_id=data['colonyId'],
        )
