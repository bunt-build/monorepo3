from setuptools import setup

setup(
    name='colonies_communication',
    version='0.0.1',
    packages=['colonies_communication'],
    install_requires=['requests==2.21.0']
)
