#!/usr/bin/env python3
import argparse

from docker import DockerClient

from ears_communication import HttpEarsClient, JobState, JobStateStatus, JobResult, JobResultStatus
from hop.execution import main
from warren.communication import HttpClientWarren, RedisQueueWarren, Job


def report_on(work_done):
    for result in work_done:
        if result:
            print('[PASSED]:', result.test_image)
        else:
            print('[FAILED]:', result.test_image)

        yield JobResult(
            test_image_name=result.test_image,
            status=JobResultStatus.SUCCESS if result else JobResultStatus.FAILED
        )


def work(args):
    queue_client = RedisQueueWarren(args.redis_url)
    warren_client = HttpClientWarren(args.warren_url)
    ears_client = HttpEarsClient(args.ears_url)
    docker_client = DockerClient.from_env()

    while True:
        job: Job = queue_client.pull()
        # Mark job as started
        ears_client.transition_state(JobState(
            id=None,
            job_id=job.id,
            status=JobStateStatus.RUNNING,
            results=[]
        ))

        # Mark job as finished
        ears_client.transition_state(JobState(
            id=None,
            job_id=job.id,
            status=JobStateStatus.FINISHED,
            results=list(report_on(main(warren_client, docker_client, job)))
        ))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test runner from backend.')
    parser.add_argument('--warren_url', type=str, default='http://warren')
    parser.add_argument('--ears_url', type=str, default='http://ears')
    parser.add_argument('--redis_url', type=str, default='redis://redis')
    work(parser.parse_args())
