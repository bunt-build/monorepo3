#!/usr/bin/env python3
import argparse

from hop.execution import main

parser = argparse.ArgumentParser(description='Bunt\'s test runner: hop.')
parser.add_argument('--container_bundle_files', type=argparse.FileType('rb'), nargs='*')
parser.add_argument('test_images', type=str, nargs='+')
args = parser.parse_args()

# TODO: Fix
for result in main(args.test_images, args.container_bundle_files):
    if result:
        print('[PASSED]:', result.test_image)
    else:
        print('[FAILED]:', result.test_image)
