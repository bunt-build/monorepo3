from typing import List

from docker.models.containers import Container
from docker.models.images import Image

from warren.communication import HttpClientWarren, Job, JobContainer
from docker import DockerClient


class ContainerManager:
    def __init__(self, docker_client: DockerClient, warren_client: HttpClientWarren, job: Job):
        self.docker_client = docker_client
        self.warren_client = warren_client
        self.job = job
        self.images: List[Image] = []

    def create_container(self, job_container: JobContainer):
        images = self.docker_client.images.load(
            data=self.warren_client.container_bundle_contents_get(job_container.bundle.id)
        )

        if len(images) != 1:
            print('Error loading container from tar. Expected 1 image.')

        return images

    def __enter__(self, *args):
        for container in self.job.containers:
            self.images += self.create_container(container)
        return self

    def __exit__(self, *args):
        existing_containers: List[Container] = self.docker_client.containers.list()

        for container in existing_containers:
            container.remove(v=True, force=True)

        for image in self.images:
            self.docker_client.images.remove(image.id)

        self.images = []
