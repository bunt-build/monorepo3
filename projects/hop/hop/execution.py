from random import randint

from docker import DockerClient

from hop.containers import ContainerManager
from hop.hopper import Hopper
from warren.communication import HttpClientWarren, Job


class ExecutionResult:
    def __init__(self, test_image: str, exit_status_code):
        self.test_image = test_image
        self.exit_status_code = exit_status_code

    def __bool__(self):
        return self.exit_status_code == 0


def main(warren_client: HttpClientWarren, docker_client: DockerClient, job: Job):
    results = []

    with ContainerManager(docker_client, warren_client, job):
        with Hopper(docker_client, f'bunt-{randint(100, 999)}') as hopper:
            for test_image_name in job.test_container_names:
                with hopper.test(test_image_name) as hop:
                    status = hop.run()
                    result = ExecutionResult(test_image_name, status['StatusCode'])
                    results.append(result)

    return results
