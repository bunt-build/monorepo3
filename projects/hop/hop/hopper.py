from docker import DockerClient
from docker.models.containers import Container
from docker.models.networks import Network


class Hop:
    def __init__(
            self,
            docker_client: DockerClient, docker_network: Network,
            test_container_docker_host: str,
            test_image: str,
    ):
        self.docker_network = docker_network

        if test_container_docker_host.startswith('unix://'):
            # Docker inside the nested dind container needs to be a unix path so mount that as a volume.
            test_container_docker_host_path = test_container_docker_host.replace('unix://', '')
            volumes = {
                test_container_docker_host_path: {'bind': test_container_docker_host_path, 'mode': 'rw'}
            }
        else:
            # Docker is not a unix path so don't need to mount.
            volumes = {}

        # Create a container
        self.container: Container = docker_client.containers.create(
            image=test_image,

            # Volumes
            volumes=volumes,

            # Configuring the application in this container
            environment={
                # Tells the bunt container how to communicate with the internal
                # docker-in-docker daemon.
                'DOCKER_HOST': test_container_docker_host,

                # Tells bunt not to create it's own network and to instead add
                # containers into the same network namespace as bunt.
                'BUNT_NETWORK': docker_network.id,

                # Bunt should not cleanup resources it creates. Hop will manage
                # this so we can pull logs and other metrics.
                'BUNT_CLEANUP_CREATED_RESOURCES': 'false'
            },

            detach=True,
        )

        # Make it discoverable by it's hostname
        docker_network.connect(self.container.id, aliases=[])

    def __enter__(self, *args):
        self.container.start()
        return self

    def __exit__(self, *args):
        self.docker_network.disconnect(self.container.id)
        self.container.remove(v=True)

    def run(self):
        status = self.container.wait()
        print(self.container.logs().decode())
        return status


class Hopper:
    """
    hop runtime environment. Setup and tear down of the docker test docker container
    """

    def __init__(self, docker_client: DockerClient, docker_network_name: str):
        self.docker_client = docker_client
        self.docker_network = self.docker_client.networks.create(
            docker_network_name,
            driver='bridge'
        )

    def test(self, test_image: str) -> Hop:
        return Hop(
            self.docker_client, self.docker_network,
            test_container_docker_host='unix:///var/run/docker.sock',
            test_image=test_image
        )

    def __enter__(self, *args):
        return self

    def __exit__(self, *args):
        # Cleanup straggler Docker containers missed in event of a crash
        for container in self.docker_network.containers:
            self.docker_network.disconnect(container)
            container.remove(v=True)
        self.docker_network.remove()
