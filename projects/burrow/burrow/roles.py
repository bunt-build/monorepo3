from enum import Enum


class Role(Enum):
    USER = 'user'
    HEADLESS = 'headless'
