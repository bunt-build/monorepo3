from abc import abstractmethod
from typing import List, Optional

from flask import request

from burrow.roles import Role
import requests
import jwt

from functools import lru_cache
from colonies_communication import HttpColonyClient


class UserSession:

    @property
    @abstractmethod
    def id(self):
        ...

    @property
    @abstractmethod
    def roles(self) -> List[Role]:
        ...

    @property
    @abstractmethod
    def name(self) -> str:
        ...

    @property
    @abstractmethod
    def email(self) -> str:
        ...

    @property
    @abstractmethod
    def magic_headers(self) -> dict:
        ...

    @staticmethod
    def load() -> Optional['UserSession']:
        if 'authorization' not in request.headers:
            return None

        token: str = request.headers['authorization']

        bearer_prefix = 'Bearer '
        if token.lower().startswith(bearer_prefix.lower()):
            # Live user token provided
            return JwtUserSession(token[len(bearer_prefix):])
        else:
            # Headless user token provided
            return HeadlessUserSession(token)


class JwtUserSession(UserSession):
    def __init__(self, token: str):
        self.jwt = jwt.decode(token, JwtUserSession.public_key(), audience='account')

    @property
    def id(self):
        return self.jwt['sub']

    @property
    def roles(self) -> List[Role]:
        roles = set()
        for role_name in self.jwt['realm_access']['roles']:
            try:
                roles.add(Role(role_name))
            except ValueError:
                continue
        return list(roles)

    @property
    def name(self) -> str:
        return self.jwt['name']

    @property
    def email(self) -> str:
        return self.jwt['email']

    @property
    def magic_headers(self) -> dict:
        return {
            'x-bunt-gateway-user-id': self.id,
            'x-bunt-gateway-user-name': self.name,
            'x-bunt-gateway-user-email': self.email,
            'x-bunt-gateway-user-roles': ','.join(role.value for role in self.roles),
        }

    @staticmethod
    @lru_cache(maxsize=None)
    def public_key() -> str:
        keycloak_config = requests.get(
            'https://keycloak:8443/auth/realms/bunt',
            verify=False
        ).json()  # TODO(jkatz): Configurable

        public_key = "-----BEGIN PUBLIC KEY-----\n"
        public_key += keycloak_config['public_key'].strip() + "\n"
        public_key += "-----END PUBLIC KEY-----\n"
        return public_key


class HeadlessUserSession(UserSession):
    def __init__(self, token: str):
        colony_client = HttpColonyClient('http://colonies')  # TODO: Make configurable
        self.token = token
        self.colony_token = colony_client.get_token(token)

    @property
    def colony(self):
        return self.colony_token.colony_id

    @property
    def id(self):
        return self.token

    @property
    def roles(self) -> List[Role]:
        return [Role.HEADLESS]

    @property
    def name(self) -> str:
        return ''

    @property
    def email(self) -> str:
        return ''

    @property
    def magic_headers(self) -> dict:
        return {
            'x-bunt-gateway-headless-id': self.id,
            'x-bunt-gateway-headless-colony': self.colony,
            'x-bunt-gateway-headless-roles': ','.join(role.value for role in self.roles),
        }
