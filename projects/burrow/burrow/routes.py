from burrow.roles import Role
from burrow import services

routes = {
    # Warren
    (Role.USER, '/jobs'): services.warren,
    (Role.USER, '/container-bundles'): services.warren,

    # Ears
    (Role.USER, '/job-states'): services.ears,

    # Colonies
    (Role.USER, '/colonies'): services.colonies,
    (Role.USER, '/colony-invites'): services.colonies,
    (Role.USER, '/colony-tokens'): services.colonies,

    # Hair Client Routes
    (Role.HEADLESS, '/container-bundles'): services.warren,  # Creating container bundles
    (Role.HEADLESS, '/job-states'): services.ears,  # Waiting for job to finish
    (Role.HEADLESS, '/jobs'): services.warren,  # Schedule job execution
    (Role.HEADLESS, '/colony-tokens'): services.colonies,  # Get the colony it is operating for
}
