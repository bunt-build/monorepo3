from typing import List, AnyStr, Tuple, Dict

import requests
from flask import request, Response, stream_with_context

from burrow.roles import Role


def can_forward_header(header_name: str):
    detectable_headers = [
        'content-encoding',
        'content-length',
        'transfer-encoding',
        'connection',
    ]

    normalized = header_name.strip().lower()

    if normalized in detectable_headers:
        return False
    if normalized.startswith('x-bunt-gateway'):
        return False
    return True


def format_headers(magic_headers: dict) -> dict:
    # Sanitize the HTTP headers provided by the client.
    headers = {
        name: value
        for name, value in request.headers.items()
        if can_forward_header(header_name=name)
    }

    # Combine provided headers with the magic headers we have
    return {
        **headers,
        **magic_headers
    }


def forward_request(magic_headers: dict, destination_url: str):
    response = requests.request(
        method=request.method,
        url=destination_url + request.full_path,
        headers=format_headers(magic_headers),
        data=iter(lambda: request.stream.read(1024 * 32), b''),
        allow_redirects=True,
        verify=False,
        stream=True,
    )
    return Response(
        stream_with_context(response.iter_content(chunk_size=None)),
        headers=list(response.headers.items())
    )


def proxy(routes: Dict[Tuple[Role, AnyStr], AnyStr], magic_headers: dict, roles: List[Role]):
    for (role_required, route_prefix), destination_url in routes.items():
        # Not authenticated enough
        if role_required not in roles:
            continue

        # Not correct prefix
        if not request.full_path.startswith(route_prefix):
            continue

        print('Proxy', route_prefix, '->', destination_url)
        return forward_request(magic_headers, destination_url)
    else:
        return Response(response='Route not found', status=404, content_type='text/plain')
