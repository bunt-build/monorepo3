from abc import ABC
from typing import List, Optional

import requests

from ears_communication.jobstate import JobState


class EarsClient(ABC):
    def states(self, job_id: str) -> List[JobState]:
        """
        List all of the states a given job as gone through
        :param job_id: ID of the job
        """

    def latest_state(self, job_id: str) -> JobState:
        """
        Find the most recent state a job has transitioned to
        :param job_id: ID of the job
        """

    def transition_state(self, job: JobState) -> JobState:
        """
        Transition to this new stat
        :param job: The newest most-current state.
        """


class HttpEarsClient(EarsClient):
    def __init__(self, url: str, token_id: Optional[str] = None):
        self.url = url
        self.token_id = token_id

    def headers(self) -> dict:
        if self.token_id:
            return {
                'Authorization': self.token_id
            }
        else:
            return {}

    def states(self, job_id: str) -> List[JobState]:
        result = requests.get(f'{self.url}/job-states', params={'equal[jobId]': job_id}, headers=self.headers())
        if not result:
            raise Exception('Failed to contact ears: ' + result.content)
        return [JobState.from_dict(data) for data in result.json()]

    def latest_state(self, job_id: str) -> JobState:
        result = requests.get(
            f'{self.url}/job-states',
            params={'equal[jobId]': job_id, 'sort[created]': 'descending', 'limit': 1},
            headers=self.headers()
        )
        if not result:
            raise Exception('Failed to contact ears: ' + result.content)
        datas = result.json()
        if not datas:
            raise Exception('No state found for ' + job_id)
        return JobState.from_dict(datas[0])

    def transition_state(self, job: JobState) -> JobState:
        result = requests.post(f'{self.url}/job-states', json=job.to_dict(), headers=self.headers())
        if not result:
            raise Exception('Failed to contact ears: ' + result.content)
        return JobState.from_dict(result.json())
