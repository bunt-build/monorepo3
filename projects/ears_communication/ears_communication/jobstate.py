from dataclasses import dataclass
from typing import Optional, List
from enum import Enum


class JobResultStatus(Enum):
    FAILED = 'failed'
    SUCCESS = 'success'


class JobStateStatus(Enum):
    QUEUED = 'queued'
    RUNNING = 'running'
    FINISHED = 'finished'


@dataclass
class JobResult:
    # id: Optional[str]
    test_image_name: str
    status: JobResultStatus

    def to_dict(self) -> dict:
        return {
            # 'id': self.id,
            'testImageName': self.test_image_name,
            'status': self.status.value,
        }

    @classmethod
    def from_dict(cls, data: dict) -> 'JobResult':
        return JobResult(
            # id=data['id'] if 'id' in data else None,
            test_image_name=data['testImageName'],
            status=JobResultStatus(data['status']),
        )


@dataclass
class JobState:
    id: Optional[str]
    job_id: str
    status: JobStateStatus
    results: List[JobResult]

    def to_dict(self) -> dict:
        return {
            'id': self.id,
            'jobId': self.job_id,
            'status': self.status.value,
            'results': [result.to_dict() for result in self.results],
        }

    @classmethod
    def from_dict(cls, data: dict) -> 'JobState':
        return JobState(
            id=data['id'] if 'id' in data else None,
            job_id=data['jobId'],
            status=JobStateStatus(data['status']),
            results=[JobResult.from_dict(result) for result in data['results']],
        )
