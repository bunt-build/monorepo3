from setuptools import setup

setup(
    name='ears_communication',
    version='0.0.1',
    packages=['ears_communication'],
    install_requires=['requests==2.21.0']
)
