from docker import DockerClient
import atexit
import tempfile
import os
from typing import List, Optional

container_bundle_files = set()


@atexit.register
def cleanup_bundled_files():
    for container_bundle_file in container_bundle_files:
        os.remove(container_bundle_file)


def save_image_with_tag(docker_client: DockerClient, image: str):
    # XXX: Can be removed after upstream fix is accepted.
    # https://github.com/docker/docker-py/pull/2458
    api = docker_client.api
    res = api._get(api._url("/images/get"), params={
        'names': [image]
    }, stream=True)
    return api._stream_raw_result(res, chunk_size=(1024 * 2048), decode=False)


def package(client: DockerClient, container_image: str) -> str:
    bundle_file = tempfile.mktemp(prefix='bunt-', suffix='.tar')

    # Garbage collection
    container_bundle_files.add(bundle_file)

    with open(bundle_file, 'wb') as handle:
        for chunk in save_image_with_tag(client, container_image):
            handle.write(chunk)

    return bundle_file
