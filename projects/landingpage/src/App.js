import React from 'react'
import {Heading, Provider, Subhead, NavLink, Relative, Absolute, Card, Text} from 'rebass'
import {
    Box,
    CallToAction,
    Contributor,
    Feature,
    Flex,
    Hero,
    Laptop,
    ScrollDownIndicator,
    Section
} from 'react-landing-page'
import Asciinema from "./Asciinema";
import SyntaxHighlighter from 'react-syntax-highlighter';
import {monokai} from 'react-syntax-highlighter/dist/esm/styles/hljs';


class CodeExample extends React.Component {
    language() {
        return 'python';
    }

    title() {
        return '';
    }

    code() {
        return '';
    }

    render() {
        return (
            <Card px={2} py={2} width={1} mt={50}>
                <Heading>{this.title()}</Heading>
                <SyntaxHighlighter lanuage={this.language()} style={monokai}>
                    {this.code()}
                </SyntaxHighlighter>
            </Card>
        );
    }
}

class CodeExampleDefineService extends CodeExample {
    title() {
        return '1. Define your Services';
    }

    code() {
        return '' +
            'class LibraryService(ContainerConfigurationRest, ContainerConfigurationHttpHealthCheck):\n' +
            '    image = \'library:latest\'\n' +
            '    host_name = \'library\'\n';
    }
}

class CodeExampleDefineTest extends CodeExample {
    title() {
        return '2. Define your Tests';
    }

    code() {
        return '' +
            '@bunt.test_case([LibraryService])\n' +
            'def test_create_book(self, library: LibraryService):\n' +
            '    book = library.create(\'/books\', json={\n' +
            '        \'title\': \'Hello\',\n' +
            '    })\n' +
            '    self.assertTrue("id" in book)\n';
    }
}

class CodeExampleRunTests extends CodeExample {

    language() {
        return 'shell';
    }

    title() {
        return '3. Run your tests';
    }

    code() {
        return '' +
            '~ bunt -d integrations list\n' +
            'integrations.test_library.LibraryTestCase.test_create_book\n' +
            '\n' +
            '~ bunt -d integrations run integrations.test_library.LibraryTestCase.test_create_book\n' +
            'Watching logs...\n' +
            'test_create_book (integrations.test_library.LibraryTestCase) ... ok\n' +
            '\n' +
            '----------------------------------------------------------------------\n' +
            'Ran 1 test in 1.628s\n' +
            '\n' +
            'OK\n' +
            '\n';
    }
}


export default class App extends React.Component {

    docsUrl() {
        return "https://gitlab.com/bunt-build/bunt/blob/master/documentation/README.md";
    }

    buntCodeUrl() {
        return "https://gitlab.com/bunt-build/bunt/";
    }

    tutorialUrl() {
        return 'https://gitlab.com/bunt-build/bunt/blob/master/documentation/overview-of-bunt-internals.md';
    }

    render() {
        return <Provider>
            <Relative pb={5}>
                <Absolute zIndex={1} left={0} right={0} top={0}>
                    <Flex bg="black" color="white" is="header" p={3}>
                        <NavLink href="/" fontSize={3}>
                            <img src="logo-bunt-head.png" alt="Bunt"/>
                        </NavLink>
                        <NavLink href={this.docsUrl()} ml='auto'>Docs</NavLink>
                        <NavLink href={this.buntCodeUrl()}>Code</NavLink>
                        <NavLink href={this.tutorialUrl()}>Tutorial</NavLink>
                    </Flex>
                </Absolute>
            </Relative>
            <Hero
                color="black"
                bg="#87cefa"
            >
                <Flex flexWrap='wrap' alignItems='center'>
                    <Flex alignItems='flex-start' width={[1, 1, 1 / 2]} p={3}>
                        <Laptop src={''} children={[
                            <Asciinema castId="GDWUlrdS1rvYw3skC8gfGJ6uq"/>
                        ]}>

                        </Laptop>
                    </Flex>
                    <Box width={[1, 1, 1 / 2]} p={3}>
                        <Heading textAlign='center'>Bunt</Heading>
                        <Subhead textAlign='center'>An Integration Testing Framework</Subhead>
                        <Flex mt={3} flexWrap='wrap' justifyContent='center'>
                            <CallToAction href={this.docsUrl()} mt={3}>Documentation</CallToAction>
                        </Flex>
                    </Box>
                </Flex>
                <ScrollDownIndicator/>
            </Hero>
            <Section>
                <Heading textAlign="center">Features of Bunt</Heading>
                <Flex flexWrap="wrap" justifyContent="center">
                    <Feature
                        icon='⏱'
                        description="Bunt provides abstraction that efficiently creates and tears down test environments
                        while allowing you to run a single, subset, or all of your tests. End to end integration tests
                        can run in a matter of seconds. This makes a development iteration code & test cycle very
                        satisfying."
                    >
                        Fast Test Execution
                    </Feature>
                    <Feature
                        icon='🥫'
                        description="Using containerization you can be sure that your tests are reproducible across
                        developer machines and are isolated. No need to worry about side effects from tests effecting
                        results from later tests."
                    >
                        Reproducible Testing Environments
                    </Feature>
                    <Feature
                        icon='🍨'
                        description="Integration tests often perform similar operations. Bunt includes instrumentation
                        for common operations like communicating with an HTTP server, connecting to databases, and
                        communicating with RESTful APIs. These abstractions are built on top of the core features bunt
                        implements and as such can be extended, modified, or reimplemented, with little effort to
                        suite your organization's needs."
                    >
                        Loads of Sugar
                    </Feature>
                    <Feature
                        icon='🔔'
                        description="Your test code and services are run within a production-like environment. Each
                        service gets it's own IP address and can see all other services. Services can talk to each
                        other using the same domain names, ports, and protocols they would use in your production
                        environment."
                    >
                        Test ALL of Your Code & Config
                    </Feature>
                </Flex>
            </Section>
            <Section
                color="black"
                bg="#87cefa"
            >
                <Heading textAlign="center">Code Examples</Heading>

                <Flex flexWrap="wrap" justifyContent="left">

                    {/*Code Examples*/}
                    <CodeExampleDefineService/>
                    <CodeExampleDefineTest/>
                    <CodeExampleRunTests/>
                </Flex>
            </Section>
            <Section>
                <Heading textAlign="center">Frequently Asked Questions</Heading>

                <Flex flexWrap="wrap" justifyContent="left">

                    <Box px={2} py={2} width={1} mt={50}>
                        <Heading>Does this replace unit testing?</Heading>
                        <Text>
                            Integration testing, and unit testing, are not magic bullets. They are tools to be used for
                            a goal. Using bunt does not mean you should not write unit tests. Having a comprehensive
                            unit test suite does not mean you don't need to write integration tests.

                            Unit tests validate the internal workings of a service. They are very coupled to the
                            implementation and design patterns of that service. Unit tests are extremely fast to execute
                            and can directly point you to which function in your code base an error or defect is coming
                            from. Integration testing will never be able to provide the speed that a unit test does.

                            Integration tests check to make sure all of the components you are using and configuring in
                            the system you are building fit together. Well made integration tests are not at all coupled
                            to the implementation of a service and should be written agnostic of the underlying
                            software. Because of this integration tests are a great way to test your services as if they
                            were being poked and prodded in your production environment. Integration tests can also be
                            used to validate implementation consistence for software written as a green field
                            replacement of an existing system. You can write a test suite, get good coverage on the
                            existing service, and then run the same test suite against your new service without changing
                            any of the test logic.
                        </Text>
                    </Box>


                    <Box px={2} py={2} width={1} mt={20}>
                        <Heading>Aren't integration tests slow?</Heading>
                        <Text>
                            Depending on what you are testing integration tests can be slow. The main question to ask is
                            whether the time spent running integration tests in your CI and development process is
                            worth the security and piece of mind the integration tests provide you. There is no one
                            answer to this question. If your engineering team members are asking if they can write some
                            integration tests they likely have a reason. It is also important to note that bunt has
                            multiple features designed to reduce the time overhead of integration testing. For
                            developers bunt allows you to run a subset of the tests in your test suite. This can save
                            a lot of time for complex systems. For your CI environment bunt also has a self hosted, MIT
                            licensed, fully free software, distributed test runner. It is in its early stages but is
                            functional enough to package and run tests. You can learn more about it and the project
                            progress <a href="https://gitlab.com/bunt-build/monorepo3">here</a>.
                        </Text>
                    </Box>

                    <Box px={2} py={2} width={1} mt={20}>
                        <Heading>Why would I use bunt when you can use Makefiles and scripts and...?</Heading>
                        <Text>
                            Bunt provides cohesive solution for integration testing. It answers questions about
                            workflow, scalability, and scope of different portions of the tooling. Doing this correctly
                            takes a lot of work. From a time perspective creating bunt took about 2 weeks of development
                            time to create the basis of bunt. Bunt is far from perfect and, to get it to a state where
                            it is "done", will likely take years. If you're ok with spending that amount of time to
                            design, develop, document, maintain, and improve your own tooling and pay the recurring
                            ramp-up penalty to train new devs that have never been exposed to your tool then you might
                            be in a position where it makes sense to write your own tooling. Bunt is Free
                            Software so feel free to look, fork, and modify at the code if you need a starting point.
                            On the other hand if your goal is not to implement an integration testing framework, and
                            instead you just want to use an integration testing framework, bunt might be what you are
                            looking for.
                        </Text>
                    </Box>


                </Flex>
            </Section>
            <Section
                color="black"
                bg="#87cefa"
            >
                <Heading textAlign="center">With Contributions from...</Heading>
                <Flex justifyContent="space-around">
                    <Contributor
                        fullName="Josh Katz"
                        title="Creator"
                        avatar="https://gitlab.com/uploads/-/system/user/avatar/2366369/avatar.png?width=400"
                    >
                        <Flex>
                            <NavLink href='https://gitlab.com/joshuakatz'>GitLab</NavLink>
                            <NavLink href='https://github.com/gravypod'>GitHub</NavLink>
                            <NavLink href='https://www.linkedin.com/in/joshuadkatz'>LinkedIn</NavLink>
                        </Flex>
                    </Contributor>
                </Flex>
            </Section>
        </Provider>;
    }
}
