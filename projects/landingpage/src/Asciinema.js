import React from 'react';

export default class Asciinema extends React.Component {
    static defaultProps = {
        width: '640px',
        height: '480px',
        castId: 'GDWUlrdS1rvYw3skC8gfGJ6uq',
    };

    constructor(props) {
        super(props);
        this.state = props;
    }


    render() {
        return (
            <a target="_blank" href={"https://asciinema.org/a/" + this.state.castId + '?autoplay=1'}>
                <img
                    alt={"terminal"}
                    src={"https://asciinema.org/a/" + this.state.castId + ".svg"}
                    width={this.state.width}
                    height={this.state.height}
                />
            </a>
        );
    }
}
