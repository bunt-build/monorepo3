from abc import ABC, abstractmethod
from typing import List

from colonies_communication import ColonyMember, Colony, ColonyMemberRole


class ColonyMemberException(Exception):
    ...


class ColonyMemberNotFound(ColonyMemberException):
    ...


class ColonyMemberRegistry(ABC):
    @abstractmethod
    def find(self, colony: Colony, user_id: str) -> ColonyMember:
        """
        :raises ColonyMemberNotFound
        :param colony:
        :param user_id:
        :return:
        """
        ...

    @abstractmethod
    def join(self, colony: Colony, user_id: str, role: ColonyMemberRole) -> ColonyMember:
        ...

    @abstractmethod
    def leave(self, colony_member: ColonyMember):
        ...

    @abstractmethod
    def memberships(self, user_id: str) -> List[ColonyMember]:
        ...

    @abstractmethod
    def list(self, colony: Colony):
        ...
