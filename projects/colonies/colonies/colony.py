from abc import ABC, abstractmethod
from typing import List

from colonies_communication.colony import Colony


class ColonyRegistry(ABC):
    @abstractmethod
    def find(self, colony_id: str) -> Colony:
        ...

    @abstractmethod
    def create(self, colony: Colony) -> Colony:
        ...
