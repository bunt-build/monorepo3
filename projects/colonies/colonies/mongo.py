from abc import ABC
from typing import List

from bson import ObjectId
from pymongo import MongoClient

from colonies.colony import ColonyRegistry
from colonies.invites import ColonyInviteRegistry
from colonies.members import ColonyMemberRegistry, ColonyMemberNotFound, ColonyMemberException
from colonies.tokens import ColonyTokenRegistry
from colonies_communication import ColonyInvite, Colony, ColonyMember, ColonyMemberRole, ColonyToken


class MongoRegistry(ABC):
    def __init__(self, url: str, database_name: str, collection_name: str):
        self.client = MongoClient(url)
        self.database = self.client.get_database(database_name)
        self.collection = self.database.get_collection(collection_name)

    @classmethod
    def document_to_data(cls, document: dict):
        data = {**document}

        if '_id' in data and data['_id']:
            data['id'] = str(data['_id'])
            del data['_id']

        return data

    @classmethod
    def data_to_document(cls, data: dict):
        document = {**data}

        if 'id' in data and data['id']:
            document['_id'] = ObjectId(data['id'])
            del document['id']

        return document


class MongoColonyMemberRegistry(ColonyMemberRegistry, MongoRegistry):

    def find(self, colony: Colony, user_id: str) -> ColonyMember:
        document = self.collection.find_one({
            'userId': user_id,
            'colonyId': colony.id,
        })

        if document is None:
            raise ColonyMemberNotFound(f'ColonyMember not found with colony {colony.id} and user {user_id}')

        return ColonyMember.from_dict(self.document_to_data(document))

    def join(self, colony: Colony, user_id: str, role: ColonyMemberRole) -> ColonyMember:
        try:
            self.find(colony, user_id)
            raise ColonyMemberException('Cannot double join a colony')
        except ColonyMemberNotFound:
            pass

        colony_member = ColonyMember(
            colony_member_id=None,
            user_id=user_id,
            colony_id=colony.id,
            role=role
        )

        result = self.collection.insert_one(self.data_to_document(colony_member.to_dict()))
        colony_member.id = str(result.inserted_id)

        return colony_member

    def leave(self, colony_member: ColonyMember):
        self.collection.find_one_and_delete({'_id': ObjectId(colony_member.id)})

    def memberships(self, user_id: str) -> List[ColonyMember]:
        colony_membership_documents = self.collection.find({
            'userId': user_id,
        })

        return [
            ColonyMember.from_dict(self.document_to_data(colony_membership_document))
            for colony_membership_document in colony_membership_documents
        ]

    def list(self, colony: Colony) -> List[ColonyMember]:
        colony_membership_documents = self.collection.find({
            'colonyId': colony.id,
        })

        return [
            ColonyMember.from_dict(self.document_to_data(colony_membership_document))
            for colony_membership_document in colony_membership_documents
        ]


class MongoColonyRegistry(ColonyRegistry, MongoRegistry):

    def find(self, colony_id: str) -> Colony:
        document = self.collection.find_one({
            '_id': ObjectId(colony_id),
        })

        return Colony.from_dict(self.document_to_data(document))

    def create(self, colony: Colony) -> Colony:
        result = self.collection.insert_one(self.data_to_document(colony.to_dict()))
        colony.id = str(result.inserted_id)
        return colony


class MongoColonyInviteRegistry(ColonyInviteRegistry, MongoRegistry):
    def create(self, colony_invite: ColonyInvite) -> ColonyInvite:
        result = self.collection.insert_one(self.data_to_document(colony_invite.to_dict()))
        if not result.acknowledged:
            print('did not ack')
        colony_invite.id = str(result.inserted_id)
        return colony_invite

    def exists(self, colony_invite_id: str) -> bool:
        return self.collection.find_one({'_id': ObjectId(colony_invite_id)}) is not None

    def use(self, colony_invite: ColonyInvite):
        self.collection.find_one_and_delete({'_id': ObjectId(colony_invite.id)})

    def get(self, colony_invite_id: str) -> ColonyInvite:
        document = self.collection.find_one({
            '_id': ObjectId(colony_invite_id)
        })
        if document is None:
            raise Exception('Colony invite does not exist')
        data = self.document_to_data(document)
        return ColonyInvite.from_dict(data)


class MongoColonyTokenRegistry(ColonyTokenRegistry, MongoRegistry):
    def find(self, colony_token_id: str) -> ColonyToken:
        token_document = self.collection.find_one({
            '_id': ObjectId(colony_token_id)
        })
        return ColonyToken.from_dict(self.document_to_data(token_document))

    def list(self, colony: Colony) -> List[ColonyToken]:
        token_documents = self.collection.find({
            'colonyId': colony.id
        })
        return [
            ColonyToken.from_dict(self.document_to_data(token_document))
            for token_document in token_documents
        ]

    def create(self, colony: Colony) -> ColonyToken:
        result = self.collection.insert_one({
            'colonyId': colony.id
        })
        return ColonyToken(
            colony_token_id=str(result.inserted_id),
            colony_id=colony.id
        )
