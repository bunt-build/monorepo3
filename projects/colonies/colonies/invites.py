from abc import ABC, abstractmethod

from colonies_communication import ColonyInvite


class ColonyInviteRegistry(ABC):
    @abstractmethod
    def create(self, colony_invite: ColonyInvite) -> ColonyInvite:
        ...

    @abstractmethod
    def exists(self, colony_invite_id: str) -> bool:
        ...

    @abstractmethod
    def use(self, colony_invite: ColonyInvite):
        ...

    @abstractmethod
    def get(self, colony_invite_id: str) -> ColonyInvite:
        ...
