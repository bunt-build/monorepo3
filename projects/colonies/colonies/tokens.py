from abc import ABC, abstractmethod
from typing import List

from colonies_communication import Colony, ColonyToken


class ColonyTokenRegistry(ABC):
    @abstractmethod
    def find(self, colony_token_id: str) -> ColonyToken:
        ...

    @abstractmethod
    def list(self, colony: Colony) -> List[ColonyToken]:
        ...

    @abstractmethod
    def create(self, colony: Colony) -> ColonyToken:
        ...
