from abc import ABC, abstractmethod

from flask import request

from colonies.members import ColonyMemberRegistry, ColonyMemberNotFound
from colonies_communication import Colony, ColonyMemberRole


class PermissionPolicy(ABC):
    @abstractmethod
    def can_create_colony_invite(self, colony: Colony) -> bool:
        """
        Is this request allowed to invite a user into the provided colony.
        :param colony:
        :return:
        """

    @abstractmethod
    def can_list_colonies_belonging_to(self, user_id: str) -> bool:
        """
        Is this request allowed to list colonies belonging to the provided User ID
        :param user_id:
        :return:
        """

    @abstractmethod
    def can_list_members_of_colony(self, colony: Colony) -> bool:
        """
        Can this request list members of this colony
        :param colony:
        :return:
        """

    @abstractmethod
    def can_get_colony_token(self, token_id: str, colony: Colony) -> bool:
        ...

    @staticmethod
    def load(colony_member_registry: ColonyMemberRegistry):
        if 'x-bunt-gateway-user-id' in request.headers:
            return UserPermissionPolicy(request.headers['x-bunt-gateway-user-id'], colony_member_registry)
        elif 'x-bunt-gateway-headless-id' in request.headers:
            return HeadlessPermissionPolicy()
        else:
            return SystemPermissionPolicy()


class HeadlessPermissionPolicy(PermissionPolicy):

    @property
    def id(self) -> str:
        return request.headers['x-bunt-gateway-headless-id']

    @property
    def colony(self) -> str:
        return request.headers['x-bunt-gateway-headless-colony']

    def can_create_colony_invite(self, colony: Colony) -> bool:
        return False

    def can_list_colonies_belonging_to(self, user_id: str) -> bool:
        return False

    def can_list_members_of_colony(self, colony: Colony) -> bool:
        return False

    def can_get_colony_token(self, token_id: str, colony: Colony) -> bool:
        return self.colony == colony.id and self.id == token_id


class UserPermissionPolicy(PermissionPolicy):
    def __init__(self, user_id: str, colony_member_registry: ColonyMemberRegistry):
        self.real_user_id = user_id
        self.colony_member_registry = colony_member_registry

    def has_role_within(self, colony: Colony, role_at_least):
        try:
            colony_member = self.colony_member_registry.find(colony, self.real_user_id)
        except ColonyMemberNotFound:
            # not even a member of the colony
            return False

        if role_at_least == ColonyMemberRole.MEMBER:
            acceptable = [ColonyMemberRole.MEMBER, ColonyMemberRole.ADMIN]
        elif role_at_least == ColonyMemberRole.ADMIN:
            acceptable = [ColonyMemberRole.ADMIN]
        else:
            raise Exception('Unknown role ' + role_at_least)

        return colony_member.role in acceptable

    def can_create_colony_invite(self, colony: Colony) -> bool:
        return self.has_role_within(colony, role_at_least=ColonyMemberRole.ADMIN)

    def can_create_colony_token(self, colony: Colony) -> bool:
        return self.has_role_within(colony, role_at_least=ColonyMemberRole.ADMIN)

    def can_list_colony_token(self, colony: Colony) -> bool:
        return self.has_role_within(colony, role_at_least=ColonyMemberRole.MEMBER)

    def can_list_members_of_colony(self, colony: Colony) -> bool:
        return self.has_role_within(colony, role_at_least=ColonyMemberRole.MEMBER)

    def can_list_colonies_belonging_to(self, user_id: str) -> bool:
        # Can only list colonies belonging to you
        return self.real_user_id == user_id

    def can_get_colony_token(self, token_id: str, colony: Colony) -> bool:
        return self.has_role_within(colony, role_at_least=ColonyMemberRole.ADMIN)


class SystemPermissionPolicy(PermissionPolicy):

    def can_get_colony_token(self, token_id: str, colony: Colony) -> bool:
        return True

    def can_create_colony_invite(self, colony: Colony) -> bool:
        return True

    def can_list_colonies_belonging_to(self, user_id: str) -> bool:
        return True

    def can_list_members_of_colony(self, colony: Colony) -> bool:
        return True
