#!/usr/bin/env bash

function domains_ext_file_alt_names()
{
    echo "DNS.1 = localhost"

    DNS_NUMBER=2
    grep -Po '([A-Za-z0-9_\.-]+.localhost)' docker-compose.yml | sort -u | while read dns_name ;
    do
        DNS_NUMBER=$((DNS_NUMBER + 1))
        echo "DNS.${DNS_NUMBER} = ${dns_name}"
    done
}

function domains_ext_file()
{
    echo "authorityKeyIdentifier=keyid,issuer"
    echo "basicConstraints=CA:FALSE"
    echo "keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment"
    echo "subjectAltName = @alt_names"
    echo "[alt_names]"
    domains_ext_file_alt_names
}


function main()
{
    SSL_DIR=data/ssl/localhost

    if [ -z $(which openssl) ];
    then
        echo "This script requires openssl. Please install the command to proceed"
        exit 1
    fi

    if [ ! -d "$SSL_DIR" ];
    then
        mkdir -p "$SSL_DIR"
        # TODO(jkatz): Don't recreate certs if they already exist and have not changed
    fi

    openssl req -x509 -nodes -new -sha256 -days 1024 -newkey rsa:2048 \
        -keyout "$SSL_DIR/root_ca.key" \
        -out "$SSL_DIR/root_ca.pem" -subj "/C=US/CN=example_root_ca"
    openssl x509 -outform pem \
        -in "$SSL_DIR/root_ca.pem" \
        -out "$SSL_DIR/root_ca.crt"
    domains_ext_file > "$SSL_DIR/domains.ext"
    openssl \
        req -new -nodes -newkey rsa:2048 \
        -keyout "$SSL_DIR/localhost.key" \
        -out "$SSL_DIR/localhost.csr" \
        -subj "/C=US/ST=NY/L=New York/O=example_certificates/CN=localhost"
    openssl x509 -req -sha256 -days 1024 \
        -in "$SSL_DIR/localhost.csr" \
        -CA "$SSL_DIR/root_ca.pem" \
        -CAkey "$SSL_DIR/root_ca.key" \
        -CAcreateserial \
        -extfile "$SSL_DIR/domains.ext" \
        -out "$SSL_DIR/localhost.crt"
}

main
